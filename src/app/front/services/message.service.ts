import { Message } from './../../shared/Models/message';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  readonly rootURL = 'http://localhost:56387/api/';
  formData: Message;
  constructor(private http: HttpClient) {}

  insertMessage() {
    return this.http.post(this.rootURL + 'Messages', this.formData);
  }
}
