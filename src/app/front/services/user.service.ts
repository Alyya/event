import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/shared/Models/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  readonly rootURL = 'http://localhost:56387/api/';
  formData: User;
  constructor(private http: HttpClient) {}

  insertUser() {
    return this.http.post(this.rootURL + 'Users', this.formData);
  }
}
