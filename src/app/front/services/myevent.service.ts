import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MyeventService {
  readonly rootURL = 'http://localhost:56387/api/';

  constructor(private http: HttpClient) {}

  //Read/Load Data-----
  loadEvents(): Observable<any> {
    return this.http.get(this.rootURL + 'Events');
  }

  postEvent(myEvent): Observable<any> {
    return this.http.post(this.rootURL + 'Events', myEvent);
  }
}
