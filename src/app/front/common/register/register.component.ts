import { User } from 'src/app/shared/Models/user';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  userList: {};
  constructor(public user_service: UserService) {}

  ngOnInit(): void {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) form.resetForm();
    this.user_service.formData = {
      UserID: 0,
      Username: '',
      Email: '',
      Password: '',
      Role: '',
      Age: '',
      Apropos: '',
    };
  }
  insertUser() {
    this.user_service.insertUser().subscribe(
      (res: any) => {
        console.log('sucess');
      },
      (err) => {
        console.log(err);
      }
    );
  }
  onsubmit(form: NgForm) {
    if (this.user_service.formData.UserID == 0) {
      //-----insert------------
      this.insertUser();

      this.resetForm();
    }
  }
}
