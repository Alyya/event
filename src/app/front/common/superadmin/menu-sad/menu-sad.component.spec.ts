import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSADComponent } from './menu-sad.component';

describe('MenuSADComponent', () => {
  let component: MenuSADComponent;
  let fixture: ComponentFixture<MenuSADComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuSADComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSADComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
