import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSADComponent } from './home-sad.component';

describe('HomeSADComponent', () => {
  let component: HomeSADComponent;
  let fixture: ComponentFixture<HomeSADComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSADComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSADComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
