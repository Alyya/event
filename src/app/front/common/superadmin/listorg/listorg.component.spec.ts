import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListorgComponent } from './listorg.component';

describe('ListorgComponent', () => {
  let component: ListorgComponent;
  let fixture: ComponentFixture<ListorgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListorgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListorgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
