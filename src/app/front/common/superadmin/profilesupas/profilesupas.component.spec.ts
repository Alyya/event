import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilesupasComponent } from './profilesupas.component';

describe('ProfilesupasComponent', () => {
  let component: ProfilesupasComponent;
  let fixture: ComponentFixture<ProfilesupasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilesupasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilesupasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
