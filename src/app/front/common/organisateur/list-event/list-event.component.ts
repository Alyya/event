import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyeventService } from 'src/app/front/services/myevent.service';
@Component({
  selector: 'app-list-event',
  templateUrl: './list-event.component.html',
  styleUrls: ['./list-event.component.css'],
})
export class ListEventComponent implements OnInit {
  EventList: {};
  constructor(private http: HttpClient, private eventService: MyeventService) {}

  ngOnInit(): void {
    this.loadEvents();
  }
  loadEvents() {
    this.eventService.loadEvents().subscribe((data) => {
      console.log(data);
      this.EventList = data as Event[];
      console.log(this.EventList);
    });
  }
}
