import { EventssComponent } from './eventss/eventss.component';
import { ContactComponent } from './../user/contact/contact.component';
import { ListEventComponent } from './list-event/list-event.component';
import { HomeorgComponent } from './homeorg/homeorg.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { OrganisateurAppComponent } from './organisateur-app/organisateur-app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
CreateEventComponent;
const routes: Routes = [
  {
    path: '',
    component: OrganisateurAppComponent,

    children: [
      { path: '', component: HomeorgComponent },
      { path: 'profile', component: HomeorgComponent },
      { path: 'createevent', component: CreateEventComponent },
      { path: 'listevent', component: EventssComponent },
      { path: 'contact', component: ContactComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrganisateursAppRoutingModule {}
