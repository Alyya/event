import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisateurAppComponent } from './organisateur-app.component';

describe('OrganisateurAppComponent', () => {
  let component: OrganisateurAppComponent;
  let fixture: ComponentFixture<OrganisateurAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisateurAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisateurAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
