export function getRegions() {
  const region = [
    'Sfax',
    'Tunis',
    'Sousse',
    'Mahdia',
    'Monastir',
    'Kairouan',
    'Gabes',
    'BEN Arous ',
    'Manouba',
    'Gafsa',
    'Bizerte',
    'Mednin',
    'Elkef',
    'Kaserine',
    'Jandouba',
    'Nabeul',
    'Zaghouan',
    'Béja',
    'Sidi Bouzid',
    'Touzeur',
    'Gabes',
    'Kebeli',
    'Tataouine',
  ];
  return region;
}
