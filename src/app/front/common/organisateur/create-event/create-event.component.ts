import { CategoriesService } from './../../../services/categories.service';
import { MyeventService } from './../../../services/myevent.service';
import { EventToCreate } from './../../../../shared/Models/EventToCreate.model';
import { Event } from './../../../../shared/Models/events.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { getRegions } from './region';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css'],
})
export class CreateEventComponent implements OnInit {
  public isCreate = true;
  public response: { dbPath: string };
  EventList: {};
  public rootURL: '';
  regions = getRegions();
  categoriesList = [];

  public eventForm: FormGroup = this.fb.group({
    region: ['Tunis'],
    CategoryID: [''],
    startDate: [''],
    endDate: [''],
    lieu: [''],
    description: [''],
    organisePar: [''],
    titre: [''],
  });

  constructor(
    /*private fb: FormBuilder,*/
    private fb: FormBuilder,
    private http: HttpClient,
    private eventService: MyeventService,
    private categoriesservice: CategoriesService
  ) {}

  ngOnInit() {
    this.categoriesservice
      .getCategoriesList()
      .subscribe((res) => (this.categoriesList = res as []));
    this.isCreate = true;
    this.loadEvents();
  }
  public onCreate() {
    let event = this.eventForm.value;
    event.ImgPath = this.createImgPath(this.response.dbPath);
    console.log(event);
    this.eventService.postEvent(event).subscribe(console.log);
  }

  public getEvents = () => {
    //this.http.get('http://localhost:56387/api/Events').subscribe(console.log);
  };

  public returnToCreate = () => {
    this.isCreate = true;
  };

  public uploadFinished = (event) => {
    this.response = event;
  };

  public createImgPath = (serverPath: string) => {
    return `${environment.apiUrl}/${serverPath}`;
  };

  loadEvents() {
    this.eventService.loadEvents().subscribe((data) => {
      this.EventList = data as Event[];
      console.log(this.EventList);
    });
  }
}
