import { MyeventService } from './../../services/myevent.service';
import { CreateEventComponent } from './create-event/create-event.component';
import { OrganisateursAppRoutingModule } from './organisateurs-app-routing.module';
import { HeaderOComponent } from './header-o/header-o.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { OrganisateurAppComponent } from './organisateur-app/organisateur-app.component';
import { Footer1Component } from './footer1/footer1.component';
import { UploadComponent } from './upload/upload.component';
import { HttpClientModule } from '@angular/common/http';
import { ListEventComponent } from './list-event/list-event.component';
import { HomeorgComponent } from './homeorg/homeorg.component';
import { SearcheventComponent } from './searchevent/searchevent.component';
import { EventssComponent } from './eventss/eventss.component';

@NgModule({
  declarations: [
    OrganisateurAppComponent,
    HeaderOComponent,
    Footer1Component,
    UploadComponent,
    CreateEventComponent,
    ListEventComponent,
    HomeorgComponent,
    SearcheventComponent,
    EventssComponent,
  ],

  imports: [
    CommonModule,
    HttpClientModule,
    OrganisateursAppRoutingModule,
    ReactiveFormsModule,
  ],
  exports: [HeaderOComponent],
  providers: [MyeventService],
})
export class OrganisateursAppModule {}
