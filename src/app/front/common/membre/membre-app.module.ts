import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MembreAppRoutingModule } from '../membre/membre-app-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { from } from 'rxjs';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MembreAppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  exports: [],
  providers: [],
})
export class MembreAppModule {}
