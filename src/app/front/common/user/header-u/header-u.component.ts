import { Component, OnInit } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/shared/Models/user';
import { UserService } from '../../../services/user.service';
import { TemplateParseError } from '@angular/compiler';
@Component({
  selector: 'app-header-u',
  templateUrl: './header-u.component.html',
  styleUrls: ['./header-u.component.css'],
})
export class HeaderUComponent implements OnInit {
  modalRef: BsModalRef;

  constructor(
    private modalService: BsModalService,
    public UserService: UserService
  ) {}

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  openModale(templatee: TemplateRef<any>) {
    this.modalRef = this.modalService.show(templatee);
  }

  ngOnInit(): void {
    this.resetForm();
  }
  resetForm(form?: NgForm) {
    if (form != null) form.resetForm();
    this.UserService.formData = {
      UserID: 0,
      Password: '',
      Username: '',
      Email: '',
      Apropos: '',
      Age: '',
      Role: '',
    };
  }
  onsubmit(form: NgForm) {
    if (this.UserService.formData.UserID == 0) {
      //-----insert------------
      this.insertUser();
    } else {
      //-----Update------------
    }
    this.resetForm();
  }

  insertUser() {
    this.UserService.insertUser().subscribe(
      (res: any) => {
        //this.toastr.success('Category Inserted', 'Sucess');
        console.log('sucess');
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
