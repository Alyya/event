import { RegisterComponent } from './../register/register.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeUComponent } from './home-u/home-u.component';
import { ContactComponent } from './contact/contact.component';
import { FoappUComponent } from './foapp-u/foapp-u.component';

const routes: Routes = [
  {
    path: '',
    component: FoappUComponent,

    children: [
      { path: '', component: HomeUComponent },
      { path: 'contact', component: ContactComponent },
      { path: 'register', component: RegisterComponent },
      /* { path: 'singup', component: ContactComponent },*/
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoAppRoutingModule {}
