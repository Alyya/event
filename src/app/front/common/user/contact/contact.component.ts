import { Message } from './../../../../shared/Models/message';
import { MessageService } from './../../../services/message.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
})
export class ContactComponent implements OnInit {
  messageList: {};

  constructor(public message_service: MessageService) {}

  ngOnInit(): void {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) form.resetForm();
    this.message_service.formData = {
      MessageID: 0,
      Username: '',
      Email: '',
      Role: '',
      Sujet: '',
      Contenu: '',
    };
  }
  InsertMessage() {
    this.message_service.insertMessage().subscribe(
      (res: any) => {
        console.log('sucess');
      },
      (err) => {
        console.log(err);
      }
    );
  }
  onsubmit(form: NgForm) {
    if (this.message_service.formData.MessageID == 0) {
      //-----insert------------
      this.InsertMessage();

      this.resetForm();
    }
  }
}
