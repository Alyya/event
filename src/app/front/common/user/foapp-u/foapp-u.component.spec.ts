import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoappUComponent } from './foapp-u.component';

describe('FoappUComponent', () => {
  let component: FoappUComponent;
  let fixture: ComponentFixture<FoappUComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoappUComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoappUComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
