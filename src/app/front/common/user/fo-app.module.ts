import { FooterComponent } from './../footer/footer.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoAppRoutingModule } from '../user/fo-app-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FoappUComponent } from './foapp-u/foapp-u.component';
import { HeaderUComponent } from './header-u/header-u.component';
import { StatComponent } from './stat/stat.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { DescriptionComponent } from './description/description.component';
import { ContactComponent } from './contact/contact.component';
import { HomeUComponent } from './home-u/home-u.component';
import { DesignComponent } from './design/design.component';

@NgModule({
  declarations: [
    FoappUComponent,
    HeaderUComponent,
    StatComponent,
    WelcomeComponent,
    DescriptionComponent,
    ContactComponent,
    HomeUComponent,
    DesignComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    FoAppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  exports: [
    HeaderUComponent,
    StatComponent,
    WelcomeComponent,
    DescriptionComponent,
    ContactComponent,
  ],
  providers: [],
})
export class FoAppModule {}
