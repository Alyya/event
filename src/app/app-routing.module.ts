import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './shared/not-found/not-found.component';

const routes: Routes = [
  {
    path: 'user',
    loadChildren: () =>
      import('./front/common/user/fo-app.module').then(
        (mod) => mod.FoAppModule
      ),
  },
  {
    path: 'membre',
    loadChildren: () =>
      import('./front/common/membre/membre-app.module').then(
        (mod) => mod.MembreAppModule
      ),
  },
  {
    path: 'organisateur',
    loadChildren: () =>
      import('./front/common/organisateur/organisateurs-app.module').then(
        (mod) => mod.OrganisateursAppModule
      ),
  },
  {
    path: 'superadmin',
    loadChildren: () =>
      import('././front/common/superadmin/superad-app-module').then(
        (mod) => mod.SuperADAppModule
      ),
  },
  {
    path: 'Admin',
    loadChildren: () =>
      import('././front/common/superadmin/admin-app-module').then(
        (mod) => mod.AdminAppModule
      ),
  },
  { path: '', redirectTo: 'user', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
