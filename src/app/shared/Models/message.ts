export class Message {
  MessageID: number;
  Username: String;
  Email: String;
  Role: String;
  Sujet: String;
  Contenu: String;
}
