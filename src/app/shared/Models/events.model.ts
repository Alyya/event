export interface Event {
  EventID: number;
  CategoryID: string;
  StartDate: string;
  Region: string;
  EndDate: string;
  Lieu: string;
  Description: string;
  OrganisePar: string;
  Titre: string;
  ImgPath: string;
}
