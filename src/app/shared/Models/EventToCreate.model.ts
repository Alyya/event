export interface EventToCreate {
  CategoryID: number;
  StartDate: string;
  Region: string;
  EndDate: string;
  Lieu: string;
  Description: string;
  OrganisePar: string;
  Titre: string;
  ImgPath: string;
}
