import { AdminAppModule } from './front/common/superadmin/admin-app-module';
import { CommonModule } from '@angular/common';
import { OrganisateursAppModule } from './front/common/organisateur/organisateurs-app.module';
import { MembreAppModule } from './front/common/membre/membre-app.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { FoAppModule } from '../app/front/common/user/fo-app.module';
import { ModalModule } from 'ngx-bootstrap';
import { RegisterComponent } from './front/common/register/register.component';
import { FormsModule } from '@angular/forms';
import { HomeSADComponent } from './front/common/superadmin/home-sad/home-sad.component';
import { MenuSADComponent } from './front/common/superadmin/menu-sad/menu-sad.component';
import { ListmessageComponent } from './front/common/superadmin/listmessage/listmessage.component';
import { CreateorgComponent } from './front/common/superadmin/createorg/createorg.component';
import { CreatecategoryComponent } from './front/common/superadmin/createcategory/createcategory.component';
import { CreateadminComponent } from './front/common/superadmin/createadmin/createadmin.component';
import { ListeventComponent } from './front/common/superadmin/listevent/listevent.component';
import { ListorgComponent } from './front/common/superadmin/listorg/listorg.component';
import { ProfilesupasComponent } from './front/common/superadmin/profilesupas/profilesupas.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    RegisterComponent,
    HomeSADComponent,
    MenuSADComponent,
    ListmessageComponent,
    CreateorgComponent,
    CreatecategoryComponent,
    CreateadminComponent,
    ListeventComponent,
    ListorgComponent,
    ProfilesupasComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ModalModule.forRoot(),
    AppRoutingModule,
    FoAppModule,
    MembreAppModule,
    FormsModule,
    ToastrModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    OrganisateursAppModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
  name: any;
  constructor(private route: ActivatedRoute) {}
  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.name = params['name'];
    });
  }
}
